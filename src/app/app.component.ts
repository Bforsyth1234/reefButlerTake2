import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import * as firebase from 'firebase/app';
import { firebaseConfig } from './credentials'


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;


  pages: Array<{title: string, component: any}>;
  rootPage: any;

  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
  ) {
    if (!this.rootPage) {
      this.rootPage = 'HomePage';
    }
    firebase.initializeApp(firebaseConfig);

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: 'HomePage' },
      { title: 'New Test', component: 'NewTestPage'},
      { title: 'Results', component: 'ResultsPage'},
      { title: 'Profile', component: 'ProfilePage'},
      { title: 'Photos', component: 'PhotosPage'},
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      const unsubscribe = firebase.auth().onAuthStateChanged(user => {
        if (!user) {
        this.rootPage = 'LoginPage';
        unsubscribe();
        } else {
        this.rootPage = 'HomePage';
        unsubscribe();
        }
        });
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
