import {Camera} from '@ionic-native/camera';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';

import { MyApp } from './app.component';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HttpModule } from '@angular/http';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireModule } from 'angularfire2';
import { FirebaseProvider } from './../providers/firebase/firebase';
import { AngularFireAuthModule }from 'angularfire2/auth';
import { AuthProvider } from '../providers/auth/auth';
import { ProfileProvider } from '../providers/profile/profile';
import { TestProvider } from '../providers/test/test';
import { ComponentsModule } from '../components/components.module';
import { ResultsProvider } from '../providers/results/results';
import { AngularFireStorageModule } from 'angularfire2/storage';





const firebaseConfig = {
    apiKey: "AIzaSyD-S1xLimxqPvc-iVXOzW04R4roO1y-KcI",
    authDomain: "reefbutler.firebaseapp.com",
    databaseURL: "https://reefbutler.firebaseio.com",
    projectId: "reefbutler",
    storageBucket: "reefbutler.appspot.com",
    messagingSenderId: "859557375562"
  };
  

@NgModule({
  declarations: [
    MyApp,
  ],
  imports: [
    BrowserModule,
    ComponentsModule,
    HttpModule,
    AngularFireDatabaseModule,
    AngularFireStorageModule,
    AngularFireModule.initializeApp(firebaseConfig),
    IonicModule.forRoot(MyApp),
    AngularFireAuthModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    ],
  providers: [
    StatusBar,
    SplashScreen,
    FirebaseProvider,
    Camera,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthProvider,
    ProfileProvider,
    TestProvider,
    ResultsProvider
  ]
})
export class AppModule {}
