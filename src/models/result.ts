export interface ResultModel {
    date: string;
    mfr: string;
    result: string;
    time: string;
    type: string;
    units: string;
}