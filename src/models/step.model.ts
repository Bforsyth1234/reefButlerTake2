export interface StepModel {
    desc: string;
    timer?: boolean;
    timeInSeconds?: number;
}