import { StepModel } from './step.model';
export interface TestModel {
    mfr: string;
    type: string;
    testId: number;
    steps: Array<StepModel>;
    result?: any;
    units: string;
}