import { AngularFireAuth } from 'angularfire2/auth';
import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { Observable } from 'rxjs/Observable';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  tests: Observable<any[]>;

  constructor(
    public navCtrl: NavController,
    db: FirebaseProvider,
    private afAuth: AngularFireAuth
  ) {
    this.tests = db.getTests().valueChanges();
  }


  ionViewDidLoad() {
    this.afAuth.authState.subscribe(data => {
      data ? null : this.goToLoginPage();
    })
  }

  goToLoginPage() {
    this.navCtrl.setRoot('LoginPage');
  }

  goToProfile(): void {
    this.navCtrl.push("ProfilePage");
  }

  goToCreate(): void {
    this.navCtrl.push('NewTestPage');
  }

  goToList(): void {
    this.navCtrl.push('ResultsPage');
  }

  goTo(pageName: string) {
    this.navCtrl.push(pageName);
  }

}
