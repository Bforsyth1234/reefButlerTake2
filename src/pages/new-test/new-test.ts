import { TestProvider } from './../../providers/test/test';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


/**
 * Generated class for the NewTestPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-new-test',
  templateUrl: 'new-test.html',
})
export class NewTestPage {
  testChoice: any;
  public testMfr;
  public testType;
  public testDate = '01/02/17';
  public testTemplate;
  public newTest;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public testProvider: TestProvider
    ) {
  }

  ionViewDidLoad() {
    this.getTestTemplate();
  }
  
  getTestTemplate() {
    this.testTemplate = this.testProvider.getTestTemplates();
  }

  createNewTest() {
    this.sendNewTest();
  }

  saveTestChoice(testChoice) {
    this.testChoice = testChoice;
  }

  sendNewTest() {
      this.navCtrl.setRoot('TestPage', {
        type: this.testChoice.type,
        mfr: this.testChoice.mfr,
        steps: this.testChoice.steps,
        units: this.testChoice.units
      });
  }
}
