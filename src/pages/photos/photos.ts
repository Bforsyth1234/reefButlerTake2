import { FirebaseProvider } from './../../providers/firebase/firebase';
import { Camera } from '@ionic-native/camera';
import { Component } from '@angular/core';
import { IonicPage, NavController, Platform } from 'ionic-angular';

/**
 * Generated class for the PhotosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-photos',
  templateUrl: 'photos.html',
})
export class PhotosPage {
  photos: {}[];
  photo: string;
  imageUrl: any;
  photoName: string;

  constructor(    
    private cameraPlugin: Camera,
    public platform: Platform,
    public navCtrl: NavController,
    public firebaseProvider: FirebaseProvider
) {
  }

  ionViewDidLoad() {
    this.getPhotos();
  }

  takePicture(): void {
    this.cameraPlugin.getPicture({
      quality: 50,
      destinationType: this.cameraPlugin.DestinationType.DATA_URL,
      encodingType: this.cameraPlugin.EncodingType.JPEG,
      mediaType: this.cameraPlugin.MediaType.PICTURE,
      allowEdit: true,
      correctOrientation: true
    }).then((tankPicture) => {
      this.photo = "data:image/jpeg;base64," + tankPicture;
      let dload = this.firebaseProvider.savePhoto(tankPicture).downloadURL();
      dload.subscribe(data => {
        this.imageUrl = data;
      })
    })
  }
  
  savePhoto() {
    this.firebaseProvider.saveImgUrl(this.imageUrl, this.photoName);
    this.imageUrl = '';
    this.photoName = '';
  }
  getPhotos() {
    this.firebaseProvider.getPhotos().valueChanges().subscribe(data => {
      this.photos = data;
    })
  }

}
