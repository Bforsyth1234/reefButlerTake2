import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ResultsPage } from './results';
import { SelectionForm } from './selection/selection-form.component';

@NgModule({
  declarations: [
    SelectionForm,
    ResultsPage,
  ],
  imports: [
    IonicPageModule.forChild(ResultsPage),
  ],
})
export class ResultsPageModule {}
