import { TestProvider } from './../../providers/test/test';
import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Chart } from 'chart.js';
import { ResultModel } from '../../models/result';

@IonicPage()
@Component({
  selector: 'page-results',
  templateUrl: 'results.html',
})
export class ResultsPage {
  public info: string = '';
  public results: Array<ResultModel> = [];
  public selectedResults: any[] = [];
  @ViewChild('lineCanvas') lineCanvas;
  lineChart: any;

  constructor(
    public navParams: NavParams,
    public navCtrl: NavController,
    public testProvider: TestProvider
  ) {
  }

  resultsSelected($event) {
    this.results = $event;
    this.getChartData();
  }

  choicesSelected($event) {
    this.info = $event;
  }

  getChartData() {
    let dataArray = [];
    let labels = [];
    let info: Array<ResultModel> = [];
    for (let i = 0; i < this.results.length; i++) {
      dataArray.push(this.results[i].result);
      labels.push(this.results[i].date);
      info.push(this.results[i]);
    }
    this.drawChart(dataArray, labels);
  }

  drawChart(data: Array<number>, labels: Array<string>) {
    this.lineChart = new Chart(this.lineCanvas.nativeElement, {
      type: 'line',
      data: {
        labels: labels,
        datasets: [
          {
            label: this.info,
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(75,192,192,0.4)",
            borderColor: "rgba(75,192,192,1)",
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: data,
            spanGaps: false,
          },
        ]
      },
      options: {
        // this hides the legend name and color. 
        legend: {
          display: false
        },
        scales: {
          xAxes: [{
            type: 'time',
            distribution: 'series'
          }]
        }
      }

    });
  }

}

