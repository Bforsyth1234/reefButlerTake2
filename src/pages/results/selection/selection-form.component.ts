import { ResultsProvider } from './../../../providers/results/results';
import { Component, Input, EventEmitter, Output } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { FormGroup } from '@angular/forms/src/model';
import { Platform } from 'ionic-angular';

@Component({
  selector: 'app-selection-form',
  templateUrl: './selection-form.component.html',
})
export class SelectionForm {
  @Input() results;
  @Output() selectedChoices = new EventEmitter;
  @Output() selectedResults = new EventEmitter;
  myform: FormGroup;
  resultMfrs: String[] = [];
  resultsType: String[] = [];
  public typeChoice: string;
  public mfrChoice: string;

  constructor(
    public resultsProvider: ResultsProvider,
    public formBuilder: FormBuilder,
    public platform: Platform
  ) {
    this.platform.ready().then(() => {
      this.getTestResults();
    });
  }

  sendSelectedChoices() {
    let choices = '';
    const selectedChoice = this.mfrChoice !== 'All' && this.typeChoice;
    selectedChoice ? choices = this.mfrChoice + ' ' + this.typeChoice : choices = this.typeChoice;
    this.selectedChoices.emit(choices);
  }

  getTestResults() {
    this.resultsProvider.getTestResults().valueChanges().subscribe(data => {
      this.results = data;
      this.getTestOptions();
    })
  }

  getTestOptions() {
    this.results.map(result => { this.pushNewTest(result); })
    this.resultMfrs = this.filterDupes(this.resultMfrs);
    this.resultsType = this.filterDupes(this.resultsType);
  }

  pushNewTest(result) {
    this.resultMfrs.push(result.mfr);
    this.resultsType.push(result.type)
  }

  // need a type
  filterDupes(items: Array<any>) {
    let noDupe = Array.from(new Set(items));
    return noDupe;
  }

  resultsSelected() {
    this.filterResults();
  }

  filterResults() {
    let filteredResults = this.results;
    this.getTestResults();
    if (this.mfrChoice && this.mfrChoice !== 'All') {
      filteredResults = filteredResults.filter(result => this.filterByMfr(result));
    }
    filteredResults = filteredResults.filter(result => this.filterByType(result));
    this.sendSelectedChoices();
    this.selectedResults.emit(filteredResults);
  }

  filterByMfr(result) {
    return result.mfr === this.mfrChoice;
  }

  filterByType(result) {
    return result.type == this.typeChoice;
  }
}
