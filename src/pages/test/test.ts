import { TestProvider } from './../../providers/test/test';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-test',
  templateUrl: 'test.html',
})
export class TestPage {
  test;
  testResult;
  matches: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public testProvider: TestProvider,
  ) {
  }
  ionViewDidLoad() {
    this.test = this.navParams.data;
    }

    saveResults() {
      this.testProvider.saveTestResults(this.test, this.testResult);
      this.navCtrl.setRoot('ResultsPage');
  }

}
