import { AngularFireAuth } from 'angularfire2/auth';
import { User } from './../../models/user';
import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';
import * as firebase from 'firebase/app';
import {
  AngularFireStorage,
  AngularFireStorageReference,
  AngularFireUploadTask
} from 'angularfire2/storage';

@Injectable()
export class FirebaseProvider {
  user: firebase.User;
  authState: Observable<firebase.User>;
  userTestsUrl: string;
  constructor(
    public afd: AngularFireDatabase,
    private afAuth: AngularFireAuth,
    private afStorage: AngularFireStorage
  ) {
      this.authState = afAuth.authState;
      this.authState.subscribe(user => {
        this.user = user;
        this.userTestsUrl = 'tests/' + this.user.uid + '/userTests/';
      });
    }

  getTests() {
      return this.afd.list('tests/');
  }
  
  addTestToUserBucket(test) {
    console.log('this.userTestsUrl = ');
    console.log(this.userTestsUrl);
    this.afd.list(this.userTestsUrl).push({test: test.testId})
  }

  getUserTests() {
    return this.afd.list(this.userTestsUrl);
    
  }

  getAuth() {
    return this.afAuth.auth.currentUser;
  }

  getUser() {
    return this.afAuth.auth.currentUser;
  }

  getUserEmail() {
    return this.user.email;
  }

  async register(user: User) {
    try {
      const result = await this.afAuth.auth.createUserWithEmailAndPassword(user.email, user.password);
      console.log(result);
    }
    catch (e) {
      console.error(e);
    }
  }

  savePhoto(imageURL: string): AngularFireUploadTask {
    let time = new Date().getTime();


    const storageRef: AngularFireStorageReference = this.afStorage
      .ref(`${this.afAuth.auth.currentUser.uid}/pictures/img_${time}.png`);
      return storageRef.putString(imageURL, 'base64', {contentType: 'image/png'});
  }
  
  saveImgUrl(url: string, name: string) {
    let date = new Date().toLocaleDateString();
    let time = new Date().toLocaleTimeString();
    const data = {
      url: url,
      time: time,
      date: date,
      name: name
    }
    this.afd.list('results/' + this.user.uid + '/userPhotos/').push(data)
  }

  getPhotos() {
    return this.afd.list('results/' + this.user.uid + '/userPhotos/');
  }

}
