import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import * as firebase from 'firebase/app';
import { AngularFireDatabase } from 'angularfire2/database';

/*
  Generated class for the ResultsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ResultsProvider {
  public resultListRef: firebase.database.Reference;

  constructor(
    public http: Http,
    public db: AngularFireDatabase
  ) {
    this.setFbData()
  }

  setFbData() {
    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        this.resultListRef = firebase.database().ref(`/userProfile/${user.uid}/results`);
      }
    });
  }
  getTestResults() {
    return this.db.list(this.resultListRef);
  }
}
