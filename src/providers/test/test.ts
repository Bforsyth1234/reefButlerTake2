import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import * as firebase from 'firebase/app';
import { AngularFireDatabase } from 'angularfire2/database';
import { TestModel } from '../../models/test.model';
import { Platform } from 'ionic-angular';

@Injectable()
export class TestProvider {
  public testListRef: firebase.database.Reference;
  public resultListRef: firebase.database.Reference;
  public newTest: TestModel;
  public testTemplates;

constructor(public db: AngularFireDatabase, public platform: Platform) {
  this.platform.ready().then(() => {
    this.setFbData();
  });
  }

  setFbData() {
    firebase.auth().onAuthStateChanged(user => { if (user) {
      this.testListRef = firebase.database().ref(`/userProfile/${user.uid}/testList`);
      this.resultListRef = firebase.database().ref(`/userProfile/${user.uid}/results`);
    }
  });
  }

  saveTestResults(test: TestModel, testResult: string) {
    let time = new Date().toLocaleTimeString();
    let date = new Date().toLocaleDateString();   
    this.resultListRef.push({
      mfr: test.mfr,
      type: test.type,
      date: date,
      time: time,
      result: testResult,
      units: test.units
    })
  }
  
  getTestTemplates() {
    return this.testTemplates = this.db.list('testTemplates').valueChanges();
  }
}
